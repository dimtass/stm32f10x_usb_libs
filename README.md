StdPeriph, USB and CMSIS libraries for stm32f10x
---
This repo contains cmake compatible libraries for use with stm32f10x. It can be used as a submodule for other projects in order to make easier to update all projects.

### Versions
- StdPeriph: v3.5.0
- USB-FS-Device: v4.1.0
- CMSIS: v5.0.2 (core_cm3.h)
